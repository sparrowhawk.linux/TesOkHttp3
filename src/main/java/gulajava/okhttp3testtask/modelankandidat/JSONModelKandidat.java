package gulajava.okhttp3testtask.modelankandidat;

/**
 * Created by Gulajava Ministudio on 1/25/16.
 */
public class JSONModelKandidat {

    private DataKandidat data;

    public JSONModelKandidat() {
    }

    public DataKandidat getData() {
        return data;
    }

    public void setData(DataKandidat data) {
        this.data = data;
    }
}
