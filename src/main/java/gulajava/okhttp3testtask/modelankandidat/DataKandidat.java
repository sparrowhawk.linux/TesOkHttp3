package gulajava.okhttp3testtask.modelankandidat;

/**
 * Created by Gulajava Ministudio on 1/25/16.
 */
public class DataKandidat {

    private ResultsCandidat results;

    public DataKandidat() {
    }

    public ResultsCandidat getResults() {
        return results;
    }

    public void setResults(ResultsCandidat results) {
        this.results = results;
    }
}
