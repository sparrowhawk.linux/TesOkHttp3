package gulajava.okhttp3testtask.internetan;

import com.fasterxml.jackson.jr.ob.JSON;

import java.util.ArrayList;
import java.util.List;

import gulajava.okhttp3testtask.modelankandidat.DataKandidat;
import gulajava.okhttp3testtask.modelankandidat.JSONModelKandidat;
import gulajava.okhttp3testtask.modelankandidat.Kandidat;
import gulajava.okhttp3testtask.modelankandidat.ResultsCandidat;
import gulajava.okhttp3testtask.modelanpartai.Data;
import gulajava.okhttp3testtask.modelanpartai.JSONModelPartai;
import gulajava.okhttp3testtask.modelanpartai.Partai;
import gulajava.okhttp3testtask.modelanpartai.ResultsPartai;
import okhttp3.Response;

/**
 * Created by Gulajava Ministudio on 1/25/16.
 */
public class Converters {


    public Converters() {
    }


    public static List<Partai> getListPartai(Response response) {

        List<Partai> listPartai = new ArrayList<>();

        try {
            JSONModelPartai jsonModelPartai = JSON.std.beanFrom(JSONModelPartai.class, response.body().byteStream());

            Data data = jsonModelPartai.getData();
            ResultsPartai resultsPartai = data.getResults();
            listPartai = resultsPartai.getParties();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listPartai;
    }


    public static List<Kandidat> getListKandidat(Response response) {

        List<Kandidat> list = new ArrayList<>();

        try {
            JSONModelKandidat jsonModelKandidat = JSON.std.beanFrom(JSONModelKandidat.class, response.body().byteStream());

            DataKandidat dataKandidat = jsonModelKandidat.getData();
            ResultsCandidat resultsCandidat = dataKandidat.getResults();
            list = resultsCandidat.getCandidates();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }


}
