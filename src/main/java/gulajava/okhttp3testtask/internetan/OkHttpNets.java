package gulajava.okhttp3testtask.internetan;

import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;

/**
 * Created by Gulajava Ministudio on 1/25/16.
 */
public class OkHttpNets {

    private static OkHttpNets ourInstance;
    private OkHttpClient mOkHttpClient;
    private String BASE_URL = "http://api.pemiluapi.org";


    public static OkHttpNets getInstance() {

        if (ourInstance == null) {
            ourInstance = new OkHttpNets();
        }

        return ourInstance;
    }

    private OkHttpNets() {

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(10, TimeUnit.SECONDS);
        builder.readTimeout(10, TimeUnit.SECONDS);
        builder.writeTimeout(10, TimeUnit.SECONDS);

        mOkHttpClient = builder.build();
    }


    public Call getRequestPartaiAsync(String apikey) {

        String urls = BASE_URL + Apis.getPartaiApis(apikey);
        Request.Builder builderReqs = new Request.Builder();
        builderReqs.addHeader("Content-Type", "application/json");
        builderReqs.url(urls);
        Request request = builderReqs.build();

        return mOkHttpClient.newCall(request);
    }

    public Call getRequestKandidatSync(String apikey) {

        String urls = BASE_URL + Apis.getKandiatApis(apikey);
        Request.Builder builderReqs = new Request.Builder();
        builderReqs.addHeader("Content-Type", "application/json");
        builderReqs.url(urls);
        Request request = builderReqs.build();

        return mOkHttpClient.newCall(request);

    }


}
