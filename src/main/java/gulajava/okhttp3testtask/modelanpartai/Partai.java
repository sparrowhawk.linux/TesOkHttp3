package gulajava.okhttp3testtask.modelanpartai;

/**
 * Created by Gulajava Ministudio on 1/25/16.
 */
public class Partai {

    private int id;
    private String nama;
    private String nama_lengkap;
    private String url_logo_medium;
    private String visi;

    public Partai() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNama_lengkap() {
        return nama_lengkap;
    }

    public void setNama_lengkap(String nama_lengkap) {
        this.nama_lengkap = nama_lengkap;
    }

    public String getUrl_logo_medium() {
        return url_logo_medium;
    }

    public void setUrl_logo_medium(String url_logo_medium) {
        this.url_logo_medium = url_logo_medium;
    }

    public String getVisi() {
        return visi;
    }

    public void setVisi(String visi) {
        this.visi = visi;
    }
}
