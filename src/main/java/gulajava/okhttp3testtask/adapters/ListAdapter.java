package gulajava.okhttp3testtask.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.bumptech.glide.Glide;

import java.util.List;

import gulajava.okhttp3testtask.modelanpartai.Partai;
import gulajava.okhttp3testtask.views.ViewHold;

/**
 * Created by Gulajava Ministudio on 1/25/16.
 */
public class ListAdapter extends ArrayAdapter<Partai> {

    private Context mContext;
    private int mIntRes;
    private List<Partai> mPartaiList;


    public ListAdapter(Context context, int resource, List<Partai> objects) {
        super(context, resource, objects);

        this.mContext = context;
        this.mIntRes = resource;
        this.mPartaiList = objects;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHold viewHold;

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(mIntRes, parent, false);
            viewHold = new ViewHold(convertView);
            convertView.setTag(viewHold);
        } else {
            viewHold = (ViewHold) convertView.getTag();
        }

        Partai partai = mPartaiList.get(position);
        String namapartai = partai.getNama_lengkap();
        String visipartai = partai.getVisi();

        String alamatgambar = partai.getUrl_logo_medium();
        Glide.with(mContext).load(alamatgambar).into(viewHold.getImageView());

        viewHold.getTextViewJudul().setText(namapartai);
        viewHold.getTextViewKet().setText(visipartai);

        return convertView;
    }
}
