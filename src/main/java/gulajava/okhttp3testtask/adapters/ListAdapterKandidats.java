package gulajava.okhttp3testtask.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

import gulajava.okhttp3testtask.modelankandidat.Kandidat;
import gulajava.okhttp3testtask.views.ViewHold;

/**
 * Created by Gulajava Ministudio on 1/25/16.
 */
public class ListAdapterKandidats extends ArrayAdapter<Kandidat> {

    private Context mContext;
    private int mIntRes;
    private List<Kandidat> mKandidatList;


    public ListAdapterKandidats(Context context, int resource, List<Kandidat> objects) {
        super(context, resource, objects);

        this.mContext = context;
        this.mIntRes = resource;
        this.mKandidatList = objects;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHold viewHold;

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(mIntRes, parent, false);
            viewHold = new ViewHold(convertView);
            convertView.setTag(viewHold);
        } else {
            viewHold = (ViewHold) convertView.getTag();
        }

        Kandidat kandidat = mKandidatList.get(position);

        String idkandidat = kandidat.getId() + "";
        String namakandidat = kandidat.getName();

        viewHold.getTextViewId().setText(idkandidat);
        viewHold.getTextViewKandidat().setText(namakandidat);

        return convertView;
    }
}
