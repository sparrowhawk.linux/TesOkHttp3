package gulajava.okhttp3testtask.aktivitas;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import bolts.CancellationTokenSource;
import bolts.Continuation;
import bolts.Task;
import gulajava.okhttp3testtask.R;
import gulajava.okhttp3testtask.adapters.ListAdapter;
import gulajava.okhttp3testtask.adapters.ListAdapterKandidats;
import gulajava.okhttp3testtask.internetan.Converters;
import gulajava.okhttp3testtask.internetan.OkHttpNets;
import gulajava.okhttp3testtask.modelankandidat.Kandidat;
import gulajava.okhttp3testtask.modelanpartai.Partai;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by Gulajava Ministudio on 1/25/16.
 */
public class HalamanUtama extends AppCompatActivity {

    private Toolbar mToolbar;
    private ActionBar mActionBar;
    private ListView mListView;

    private ProgressDialog mProgressDialog;
    private Call mCall;
    private String APIKEY = "37784f22ef9ed0683765c96c647a04ea";
    private List<Partai> mPartaiList;
    private List<Kandidat> mKandidatList;

    private CancellationTokenSource mCancellationTokenSource;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.halamanutama);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        HalamanUtama.this.setSupportActionBar(mToolbar);

        mActionBar = HalamanUtama.this.getSupportActionBar();
        assert mActionBar != null;
        mActionBar.setTitle(R.string.app_name);
        mActionBar.setSubtitle("Pemilu API");

        mListView = (ListView) findViewById(R.id.list);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        HalamanUtama.this.getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {

            case R.id.action_segarsasync:

                segarinDataAsync();
                return true;

            case R.id.action_segars:

                segarinDataKandidatSync();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void segarinDataAsync() {

        tampilProgressASync("Ambil data asinkronus");

        mCancellationTokenSource = new CancellationTokenSource();
        mCall = OkHttpNets.getInstance().getRequestPartaiAsync(APIKEY);

        mCall.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                mProgressDialog.dismiss();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                if (response.isSuccessful()) {

                    Task.callInBackground(new Callable<List<Partai>>() {
                        @Override
                        public List<Partai> call() throws Exception {

                            return Converters.getListPartai(response);
                        }
                    })
                            .continueWith(new Continuation<List<Partai>, Object>() {
                                @Override
                                public Object then(Task<List<Partai>> task) throws Exception {

                                    mPartaiList = task.getResult();
                                    mProgressDialog.dismiss();

                                    ListAdapter listAdapter = new ListAdapter(HalamanUtama.this, R.layout.desain_baris, mPartaiList);
                                    mListView.setAdapter(listAdapter);

                                    return null;
                                }
                            }, Task.UI_THREAD_EXECUTOR);

                } else {
                    mProgressDialog.dismiss();
                    Toast.makeText(HalamanUtama.this, "Gagal ambil data", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void segarinDataKandidatSync() {

        tampilProgressSync("Ambil data sinkronus");
        mCall = OkHttpNets.getInstance().getRequestKandidatSync(APIKEY);

        mCancellationTokenSource = new CancellationTokenSource();

        Task.callInBackground(new Callable<Response>() {
            @Override
            public Response call() throws Exception {

                return mCall.execute();

            }
        }, mCancellationTokenSource.getToken())
                .continueWith(new Continuation<Response, List<Kandidat>>() {
                    @Override
                    public List<Kandidat> then(Task<Response> task) throws Exception {

                        Response response = task.getResult();
                        List<Kandidat> list = new ArrayList<>();

                        if (response.isSuccessful()) {
                            list = Converters.getListKandidat(response);
                        }

                        return list;
                    }
                }, Task.BACKGROUND_EXECUTOR, mCancellationTokenSource.getToken())

                .continueWith(new Continuation<List<Kandidat>, Object>() {
                    @Override
                    public Object then(Task<List<Kandidat>> task) throws Exception {

                        mKandidatList = task.getResult();
                        ListAdapterKandidats listAdapterKandidats = new ListAdapterKandidats(HalamanUtama.this, R.layout.desain_bariskandidat, mKandidatList);
                        mListView.setAdapter(listAdapterKandidats);

                        if (mKandidatList.size() == 0) {
                            Toast.makeText(HalamanUtama.this, "Gagal ambil data", Toast.LENGTH_SHORT).show();
                        }

                        mProgressDialog.dismiss();

                        return null;
                    }
                }, Task.UI_THREAD_EXECUTOR, mCancellationTokenSource.getToken());

    }


    private void tampilProgressSync(String pesan) {

        mProgressDialog = new ProgressDialog(HalamanUtama.this);
        mProgressDialog.setMessage(pesan);
        mProgressDialog.setOnCancelListener(mOnCancelListenerSync);
        mProgressDialog.show();

    }

    ProgressDialog.OnCancelListener mOnCancelListenerSync = new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialogInterface) {

            if (mCall != null && !mCall.isCanceled()) {
                mCall.cancel();
            }
            mCancellationTokenSource.cancel();
        }
    };


    private void tampilProgressASync(String pesan) {

        mProgressDialog = new ProgressDialog(HalamanUtama.this);
        mProgressDialog.setMessage(pesan);
        mProgressDialog.setOnCancelListener(mOnCancelListener);
        mProgressDialog.show();

    }

    ProgressDialog.OnCancelListener mOnCancelListener = new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialogInterface) {

            if (mCall != null && !mCall.isCanceled()) {
                mCall.cancel();
            }
        }
    };


}
